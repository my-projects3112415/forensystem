<?php

namespace Database\Seeders;

use App\Models\Comment;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 0; $i < 1000; $i++) {
            $comment = new Comment();
            $comment->content = $faker->realTextBetween(100,300,5);
            $comment->user_id = $faker->numberBetween(1,40);
            $comment->topic_id = $faker->numberBetween(1,90);
            $comment->save();
        }
    }
}

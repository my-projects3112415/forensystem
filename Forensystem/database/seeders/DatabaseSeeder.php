<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $roles = new RoleSeeder();
        $boards = new BoardSeeder();
        $users = new UsersSeeder();
        $topics = new TopicSeeder();
        $comments = new CommentSeeder();


        $roles->run();
        $boards->run();
        $users->run();
        $topics->run();
        $comments->run();
    }
}

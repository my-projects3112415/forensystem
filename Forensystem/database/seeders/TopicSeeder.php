<?php

namespace Database\Seeders;

use App\Models\Topic;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        for ($i = 1; $i < 91; $i++) {
            $topic = new Topic();
//            $topic->title = $faker->realTextBetween(50,100,5);
            $topic->title = $faker->sentence(10);
            $topic->board_id = $faker->numberBetween(1,20);
            $topic->user_id = $faker->numberBetween(1,40);
            $topic->save();
        }
    }
}

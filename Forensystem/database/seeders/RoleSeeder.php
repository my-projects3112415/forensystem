<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = new Role();
        $user = new Role();
        $mod = new Role();

        $admin->type = "ADMIN";
        $user->type = "USER";
        $mod->type = "MOD";

        $admin->save();
        $user->save();
        $mod->save();
    }
}

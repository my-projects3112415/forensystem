<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();

        $admin = User::create([
            'name' => "ghaith",
            'email' => "ghaith@hotmail.com",
            'email_verified_at' => $faker->boolean(70) ? now() : null, // 70% chance of having a verified email
            'password' => bcrypt('12345'), // default password for testing
            'profilePic' => $faker->imageUrl(),
            'birthday' => $faker->date,
            'biography'=> $faker->realTextBetween(),
            'remember_token' => $faker->randomAscii,
            'created_at' => $faker->dateTimeBetween('-1 year', now()), // Random creation date in the last year
            'updated_at' => now(),
        ]);
        $admin->roles()->attach([1, 3]);


        for ($i = 2; $i < 41; $i++) {
            $users = User::create([
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => $faker->boolean(70) ? now() : null, // 70% chance of having a verified email
                'password' => bcrypt('12345'), // default password for testing
                'profilePic' => $faker->imageUrl(),
                'biography'=> $faker->realTextBetween(),
                'birthday' => $faker->date,
                'remember_token' => $faker->randomAscii,
                'created_at' => $faker->dateTimeBetween('-1 year', now()), // Random creation date in the last year
                'updated_at' => now(),
            ]);

            if ($i % 5 == 0) {
                $users->roles()->attach([2,3]);
            }
            $users->roles()->attach([2]);
        }
    }
}

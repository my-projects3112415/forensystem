@extends('layout.sub')
@section('body')

    <div class="d-flex justify-content-center">
        <div class="alert alert-danger m-5 col-7 shadow">
            <h3>{{ $notificationData->commentBy }} commented on your Topic</h3>
            <p>{{ $notificationData->topicTitle}}</p>
            <hr>
            <h2>
                {{ $notificationData->comment}}
            </h2>
        </div>
    </div>
@endsection

@extends('layout.sub')
@section('body')

    <div class="d-flex justify-content-center">
        <div class="alert alert-danger m-5 col-7 shadow">
            <h3>Your Reset Password Code</h3>
            <h5>{{$resetCode}}</h5>
        </div>
    </div>
@endsection

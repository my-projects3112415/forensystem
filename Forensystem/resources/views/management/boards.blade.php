@extends('layout.main')
@section('body')


    <x-messages-form/>

    <div class="d-flex justify-content-around">

        <form action="{{ route('board.store')}} " method="post" class="mx-2">
            @csrf
            <div class="input-group mb-3 my-3">
                    <button class="btn btn-success" type="submit" id="button-addon1">Add</button>
                    <input name="name" type="text" class="form-control"
                           placeholder="Board Name" aria-label="Example text with button addon"
                           aria-describedby="button-addon1">
            </div>
        </form>

        @if(isset($boardToEdit))
            <form action="{{ route('board.update',$boardToEdit->id)}} " method="post" class="mx-2">
                @csrf
                @method('PUT')
                <div class="input-group mb-3 my-3">
                    <button class="btn btn-warning" type="submit" id="button-addon1">Edit</button>
                    <input name="name" type="text" class="form-control"
                           placeholder="Board Name" aria-label="Example text with button addon"
                           aria-describedby="button-addon1" value="{{$boardToEdit->name}}">
                </div>
            </form>
        @endif

    </div>

    <div class="container text-center">
            <div class="row">
                @foreach($boards as $board)
                    <x-board-box :board="$board" />
                @endforeach
            </div>
    </div>

    <div class="d-flex justify-content-center my-3">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="{{$boards->previousPageUrl()}}">Previous</a></li>
                <li class="page-item"><a class="page-link" href="{{$boards->nextPageUrl()}}">Next</a></li>
            </ul>
        </nav>
    </div>
@endsection

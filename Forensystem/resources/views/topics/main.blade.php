@extends('layout.main')
@section('body')

    <div class="container">
        <x-messages-form/>

        <x-topic-title :topic="$topic"/>

        <div class="d-flex flex-column mx-5">
            @foreach($topic->comments as $comment)
                <x-comment-box :comment="$comment" />
            @endforeach

            @auth
                <x-comment-form topicID="{{$topic->id}}" comment="{{null}}"/>
            @endauth
        </div>
    </div>
@endsection

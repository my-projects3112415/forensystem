@php
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Session;

@endphp
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <title>Gh-SO</title>
</head>
@vite(['resources/js/app.js'])

<nav class="navbar navbar-expand-lg bg-dark">
    <div class="container-fluid">
        @auth
            <a class="navbar-brand text-white me-5" href="{{route('get.Account')}}">
                @if(Auth::user()->profilePic == null)
                    <i class="bi bi-person-circle text-white fs-2"></i>
                @else
                    <img class="rounded-5" src="{{asset('storage/images/'.Auth::user()->profilePic)}}"
                         alt="Picture"
                         style="max-width: 45px; max-height:45px">
                @endif
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        @endauth

        @if(!Auth::check())
            <div>
                <a class="nav-link text-white" href="{{route('loginPage')}}">Login</a>
            </div>
        @endif

        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav row-cols-lg-1">
                <li class="nav-item">
                    <a class="nav-link active text-white" aria-current="page" href="{{route('topic.index')}}">Home</a>
                </li>

                @auth
                    @can('isAdmin')
                        <li class="nav-item dropdown">
                            <a class="nav-link text-white" role="button" data-bs-toggle="dropdown"
                               aria-expanded="false">
                                Management
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{route('users.index')}}">Users</a></li>
                                <li><a class="dropdown-item" href="{{route('board.index')}}">Boards</a></li>
                            </ul>
                        </li>
                    @endcan
                    <li class="nav-item dropdown">
                        <a class="nav-link text-white fs-5" role="button" data-bs-toggle="dropdown"
                           aria-expanded="false">
                            <i class="bi bi-bell-fill position-relative">
                                @if(Session::get('unread') > 0)
                                    <div class="position-absolute translate-middle badge rounded-pill bg-danger">
                                        {{Session::get('unread')}}
                                    </div>
                                @endif
                            </i>
                        </a>

                        <ul class="dropdown-menu">
                            @if(Session::get('notification')->count() > 0)
                                <li>
                                    <div class="d-flex justify-content-around">
                                        <div>
                                            <a class="dropdown-item" href="{{route('notification.index')}}">
                                                <i class="bi bi-arrow-clockwise"></i>
                                            </a>
                                        </div>
                                        <div>
                                            <a class="dropdown-item" href="{{route('notification.options','+')}}">
                                                <i class="bi bi-check2-all"></i>
                                            </a>
                                        </div>
                                        <div>
                                            <a class="dropdown-item" href="{{route('notification.options','-')}}">
                                                <i class="bi bi-trash"></i>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            @else

                                <li>
                                    <p class="dropdown-item" >No Notifications</p>
                                </li>
                            @endif

                            @foreach( Session::get('notification') as $not )
                                <li>
                                    <div class="d-flex justify-content-between">
                                        @if(!$not->read)
                                            <a class="dropdown-item text-primary-emphasis" href="{{route('notification.show',$not->id)}}">
                                                {{$not->topic->board->name}}
                                            </a>
                                        @else
                                            <a class="dropdown-item" href="{{route('notification.show',$not->id)}}">
                                                {{$not->topic->board->name}}
                                            </a>
                                        @endif
                                        <div class="d-flex justify-content-between">
                                            <form class="mx-3" action="{{route('notification.destroy',$not->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn">
                                                    <i class="bi bi-trash3 text-danger fs-5"></i>
                                                </button>
                                            </form>
                                            <a href="{{route('notification.edit',$not->id)}}"><i class="bi bi-check2-circle text-success fs-4 mx-2"></i></a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </li>
                    <li class="nav-item text-danger">
                        <a class="nav-link" href="{{route('logout')}}">
                            <i class="bi bi-box-arrow-right text-danger fs-5"></i>
                        </a>
                    </li>
            </ul>
            @endauth

        </div>
    </div>
</nav>
@section('body')
@show
</html>

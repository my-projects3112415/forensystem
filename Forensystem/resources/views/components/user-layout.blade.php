
    <section class="search-result-item">
        <a class="image-link" href="#"><img class="image" src="{{asset('storage/images/'.$user->profilePic)}}">
        </a>
        <div class="search-result-item-body">
            <div class="row">
                <div class="col-sm-9">
                    <h4 class="search-result-item-heading"><a href="#">{{$user->name}}</a>
                        @if($user->roles->contains('type','ADMIN'))
                            <span class="badge bg-danger fw-normal pull-right">ADMIN</span>
                        @elseif($user->roles->contains('type','MOD'))
                            <span class="badge bg-warning fw-normal pull-right">MODIFIER</span>
                        @endif

                    </h4>
                    <p class="info">{{$user->birthday}}</p>
                    <p class="description">{{$user->biography}}</p>
                </div>
                <div class="col-sm-3 text-align-center">
                    <p class="value3 mt-sm">{{$user->comments->count()}}</p>
                    <p class="fs-mini text-muted">Comments</p>
                    <a class="btn btn-primary btn-info btn-sm" href="{{route('users.show',$user->id)}}">
                        Edit Profile</a>
                </div>
            </div>
        </div>
    </section>


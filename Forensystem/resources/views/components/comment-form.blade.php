<div class="alert bg-dark">
    @if( $comment == null )
        <form action="{{route('comment.store')}}" method="post">
            @csrf
            <input name="topicID" type="hidden" value="{{$topicID}}">
            <div class="form-floating">
                <textarea name="commentContent" class="form-control" placeholder="Leave a comment here"
                          id="floatingTextarea2"
                          style="height: 100px"></textarea>
                <label for="floatingTextarea2">Comments</label>
            </div>
            <div class="d-flex justify-content-end my-2">
                <button class="btn btn-primary" type="submit">Commit</button>
            </div>
        </form>
    @else
        @vite(['resources/js/app.js'])
            <form action="{{route('comment.update',$comment->id)}}" method="post">
                @csrf
                @method('PUT')
                <div class="form-floating">
                <textarea name="commentContent" class="form-control" placeholder="Leave a comment here"
                          id="floatingTextarea2"
                          style="height: 100px">{{$comment->content}}</textarea>
                    <label for="floatingTextarea2">Comments</label>
                </div>
                <div class="d-flex justify-content-end my-2">
                    <button class="btn btn-primary" type="submit">Commit</button>
                </div>
            </form>
    @endif
</div>

<div class="cookies-card m-2">
    <p class="cookie-heading">{{$board->name}}</p>
    <p class="cookie-para">Topics in this Board: {{$board->topics->count()}}</p>
    <p class="cookie-para">
        @php
        $sum= 0;
        foreach ($board->topics as $topic){
            $comments = $topic->comments->count();
            $sum +=  $comments;
        }
        @endphp
        Sum of Comments: {{$sum}}
    </p>

    <div class="d-flex justify-content-around">
        <a href="{{route('board.show',$board->id)}}" class="btn btn-warning">Edit</a>

        <form action="{{route('board.destroy',$board->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Delete</button>
        </form>
    </div>

</div>



<style>
    .cookies-card {
        width: 280px;
        height: fit-content;
        background-color: rgb(255, 250, 250);
        border-radius: 10px;
        border: 1px solid rgb(206, 206, 206);
        display: flex;
        flex-direction: column;
        padding: 20px;
        gap: 15px;
        position: relative;
        font-family: Arial, Helvetica, sans-serif;
        box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.066);
    }

    .cookie-heading {
        color: rgb(34, 34, 34);
        font-weight: 800;
    }
    .cookie-para {
        font-size: 11px;
        font-weight: 400;
        color: rgb(51, 51, 51);
    }

</style>

@extends('layout.main')
@section('body')

    <x-messages-form/>

    @if(isset($topic))
        <form action="{{route('topic.update',$topic->id)}}" method="post">
            @csrf
            @method('PUT')
            <div class="form-floating m-3">
            <textarea name="title" class="form-control"
                      placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"
            >{{$topic->title}}</textarea>
                <label for="floatingTextarea2">Topic Content</label>
            </div>

            <select name="board" class="form-select m-3" multiple aria-label="multiple select example">
                @foreach($boards as $board)
                    <option value="{{$board->id}}">{{$board->name}}</option>
                @endforeach
            </select>


            <div class="d-flex justify-content-around">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{route('topic.show',$topic->id)}}" class="btn btn-outline-dark">Back</a>
            </div>
        </form>
    @else
        <form action="{{route('topic.store')}}" method="post">
            @csrf
            <div class="form-floating m-3">
            <textarea name="title" class="form-control"
                      placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"
            >{{old('title')}}</textarea>
                <label for="floatingTextarea2">Topic Content</label>
            </div>

            <select name="board" class="form-select m-3" multiple aria-label="multiple select example">
                @foreach($boards as $board)
                    <option value="{{$board->id}}">{{$board->name}}</option>
                @endforeach
            </select>


            <div class="d-flex justify-content-around">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{route('topic.index')}}" class="btn btn-outline-dark">Back</a>
            </div>
        </form>
    @endif

@endsection

<form class="row g-3 mx-2 my-1" action="{{route('search')}}" method="post">
    @csrf
    <div class="col-auto">
        <label for="keyWord" class="visually-hidden">Password</label>
        <input type="text" name="keyWord" value="{{old('keyWord')}}"
               class="form-control" id="keyWord" placeholder="Search">
    </div>
    <div class="col-auto">
        <button type="submit" class="btn btn-primary mb-3">Search</button>
    </div>
</form>


{{--<form action="{{route('search')}}" method="post" class="col-8">--}}
{{--    @csrf--}}
{{--    <input class="form-control" type="text" name="keyWord" id="" value="{{old('keyWord')}}">--}}
{{--    <button type="submit">Search</button>--}}
{{--</form>--}}


<div class="card mb-3">
    <div class="row g-2">
        @if(!empty($comment->user->profilePic))
            <div class="col-md-2">
                <img src="{{asset('storage/images/'.$comment->user->profilePic)}}"
                     class="img-fluid rounded-start" alt="Picture">
            </div>
        @else
            <div class="col-md-2 d-flex justify-content-center">
                <i class="bi bi-person-circle fs-1"></i>
            </div>
        @endif
        <div class="col-md-9">
            <div class="card-body">
                <h5 class="card-title">{{$comment->user->name}}</h5>
                <p class="card-text">{{$comment->content}}</p>
                <p class="card-text"><small class="text-muted">Created
                        at {{$comment->created_at->format('Y-m-d')}}</small>
                </p>
            </div>
        </div>
        @can('edit-comment',[$comment])
            <div class="col-md-1 mt-4">
                <div class="dropdown">
                    <i class="bi bi-pencil fs-5 text-primary" type="button" data-bs-toggle="dropdown"></i>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{route('comment.edit',$comment->id)}}">Edit</a>
                        </li>
                        <li>
                            <form action="{{route('comment.destroy',$comment->id)}}"
                                  method="post">
                                @csrf
                                @method('DELETE')
                                <button class="dropdown-item text-danger" type="submit">Delete</button>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        @endcan
    </div>
</div>

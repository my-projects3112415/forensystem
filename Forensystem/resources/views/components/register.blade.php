@extends('layout.sub')
@section('body')

    <x-messages-form/>

    @if(isset($user))
        <x-edit-profile-form :user="$user" />
    @else
        <x-register-form/>
    @endif

@endsection

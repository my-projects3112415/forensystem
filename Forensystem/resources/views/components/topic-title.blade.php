
<div class="m-3">

    @can('edit-topic',$topic)
    <div class="d-flex justify-content-end my-3">
            <div class="dropdown">
                    <i class="bi bi-pencil-square fs-3" type="button" data-bs-toggle="dropdown"></i>
                <ul class="dropdown-menu">
                    <li>
                        <a class="dropdown-item" href="{{route('topic.edit',$topic->id)}}">Edit</a>
                    </li>
                    <li>
                        <form action="{{route('topic.destroy',$topic->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="dropdown-item text-danger" type="submit">Delete</button>
                        </form>
                    </li>
                </ul>
            </div>
    </div>
    @endcan


    <div class="advice-container">
        <p class="paragraph">{{$topic->board->name}}</p>

        <div class="advice-details">“{{$topic->title}}”</div>

        <div class="pattern-divider">
            <svg width="295" height="16" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd">
                    <path fill="#4F5D74" d="M0 8h122v1H0zM173 8h122v1H173z"></path>
                    <g transform="translate(138)" fill="#CEE3E9"><rect width="6" height="16" rx="3"></rect><rect x="14" width="6" height="16" rx="3"></rect></g>
                </g>
            </svg>
        </div>
    </div>
</div>

<style>
    .advice-container {
        font-family: "Manrope", sans-serif;
        display: flex;
        flex-direction: column;
        align-items: center;
        text-align: center;
        width: 100%;
        height: auto;
        background-color: hsl(217, 19%, 24%);
        border-radius: 10px;
        padding: 30px 16px 0 16px;
    }

    .paragraph {
        font-size: 17px;
        background-image: linear-gradient(to right, hsl(150, 100%, 66%),#C031B5);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        text-transform: uppercase;
        font-weight: 500;
        letter-spacing: 1px;
        /*Reason: there are some contrast issues*/
    }

    .advice-details {
        color: hsl(193, 38%, 86%);
        font-size: 20px;
        font-weight: 500;
        margin: 30px 0;
        line-height: 25px;
        text-wrap: balance;
        letter-spacing: 1px;
    }

    .pattern-divider path {
        fill: hsl(217, 19%, 38%);
    }
    .pattern-divider{
        margin-bottom: 15px;
    }

</style>

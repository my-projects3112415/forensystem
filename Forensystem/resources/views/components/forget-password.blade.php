@extends('layout.sub')
@section('body')

    <x-messages-form/>

    @if(  isset($isCodeSent)  || $errors->any())
        <x-verify-code-form/>
    @else
        <x-reset-email-form/>
    @endif

@endsection

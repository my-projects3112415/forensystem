{{--    Messages--}}
<div class="d-flex justify-content-center">
    @if (Session::has('message'))
        <div class="alert alert-success m-2 shadow">
            <ul>
                {{Session::get('message')}}
            </ul>
        </div>
    @endif
</div>

{{--    Errors --}}
<div class="d-flex justify-content-center">
    @if ($errors->any())
        <div class="alert alert-danger m-2 shadow">
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="my-2">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>

<div class="d-flex justify-content-center my-3">

    <div class="card">
        <div class="profileImage">
            @if($user->profilePic != null)
                <img src="{{asset('storage/images/'.$user->profilePic)}}"
                     class="rounded border border-1"
                     style="max-width: 100%"
                     alt="Profile Picture">
            @else
                <svg viewBox="0 0 128 128"><circle r="60" fill="transparent" cy="64" cx="64">
                    </circle>
                    <circle r="48" fill="transparent" cy="64" cx="64">

                    </circle>
                    <path fill="#191919" d="m64 14a32 32 0 0 1 32 32v41a6 6 0 0 1 -6 6h-52a6 6 0 0 1 -6-6v-41a32 32 0 0 1 32-32z">

                    </path>
                    <path opacity="1" fill="#191919" d="m62.73 22h2.54a23.73 23.73 0 0 1 23.73 23.73v42.82a4.45 4.45 0 0 1 -4.45 4.45h-41.1a4.45 4.45 0 0 1 -4.45-4.45v-42.82a23.73 23.73 0 0 1 23.73-23.73z"></path>
                    <circle r="7" fill="#fbc0aa" cy="65" cx="89"></circle>
                    <path fill="#4bc190" d="m64 124a59.67 59.67 0 0 0 34.69-11.06l-3.32-9.3a10 10 0 0 0 -9.37-6.64h-43.95a10 10 0 0 0 -9.42 6.64l-3.32 9.3a59.67 59.67 0 0 0 34.69 11.06z">

                    </path><path opacity=".3" fill="#356cb6" d="m45 110 5.55 2.92-2.55 8.92a60.14 60.14 0 0 0 9 1.74v-27.08l-12.38 10.25a2 2 0 0 0 .38 3.25z"></path>
                    <path opacity=".3" fill="#356cb6" d="m71 96.5v27.09a60.14 60.14 0 0 0 9-1.74l-2.54-8.93 5.54-2.92a2 2 0 0 0 .41-3.25z"></path><path fill="#fff" d="m57 123.68a58.54 58.54 0 0 0 14 0v-25.68h-14z"></path>
                    <path stroke-width="14" stroke-linejoin="round" stroke-linecap="round" stroke="#fbc0aa" fill="none" d="m64 88.75v9.75">

                    </path><circle r="7" fill="#fbc0aa" cy="65" cx="39">

                    </circle>
                    <path fill="#ffd8ca" d="m64 91a25 25 0 0 1 -25-25v-16.48a25 25 0 1 1 50 0v16.48a25 25 0 0 1 -25 25z"></path>
                    <path fill="#191919" d="m91.49 51.12v-4.72c0-14.95-11.71-27.61-26.66-28a27.51 27.51 0 0 0 -28.32 27.42v5.33a2 2 0 0 0 2 2h6.81a8 8 0 0 0 6.5-3.33l4.94-6.88a18.45 18.45 0 0 1 1.37 1.63 22.84 22.84 0 0 0 17.87 8.58h13.45a2 2 0 0 0 2.04-2.03z"></path>
                    <path style="fill:none;stroke-linecap:round;stroke:#fff;stroke-miterlimit:10;stroke-width:2;opacity:.1" d="m62.76 36.94c4.24 8.74 10.71 10.21 16.09 10.21h5"></path>
                    <path style="fill:none;stroke-linecap:round;stroke:#fff;stroke-miterlimit:10;stroke-width:2;opacity:.1" d="m71 35c2.52 5.22 6.39 6.09 9.6 6.09h3"></path>
                    <circle r="3" fill="#515570" cy="62.28" cx="76"></circle>
                    <circle r="3" fill="#515570" cy="62.28" cx="52"></circle>
                    <ellipse ry="2.98" rx="4.58" opacity=".1" fill="#f85565" cy="69.67" cx="50.42"></ellipse>
                    <ellipse ry="2.98" rx="4.58" opacity=".1" fill="#f85565" cy="69.67" cx="77.58"></ellipse>
                    <g stroke-linejoin="round" stroke-linecap="round" fill="none"><path stroke-width="4" stroke="#fbc0aa" d="m64 67v4"></path>
                        <path stroke-width="2" stroke="#515570" opacity=".2" d="m55 56h-9.25"></path><path stroke-width="2" stroke="#515570" opacity=".2" d="m82 56h-9.25"></path>
                    </g><path opacity=".4" fill="#f85565" d="m64 84c5 0 7-3 7-3h-14s2 3 7 3z"></path>
                    <path fill="#f85565" d="m65.07 78.93-.55.55a.73.73 0 0 1 -1 0l-.55-.55c-1.14-1.14-2.93-.93-4.27.47l-1.7 1.6h14l-1.66-1.6c-1.34-1.4-3.13-1.61-4.27-.47z">
                    </path>
                </svg>
            @endif
        </div>
    </div>

</div>


<div class="bg-dark-subtle p-2 m-4 rounded">
    <div class="d-flex justify-content-end">
        @if($user->isActive)
            <form class="mx-2" action="{{route('users.destroy',$user->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Deactivate Account</button>
            </form>
        @else
            <form class="mx-2" action="{{route('users.destroy',$user->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-success" type="submit">Activate Account</button>
            </form>
        @endif

    </div>


    <form action="{{route('edit.Account')}}" method="post" class="m-4 border-dark-subtle border-4"
          enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <input name="id" type="hidden" value="{{$user->id}}">

        <div class="mb-3">
            <label for="username" class="form-label">Username</label>
            <input name="username" type="text" class="form-control" id="username" value="{{$user->name}}">
        </div>

        <div class="mb-3">
            <label for="biography" class="form-label">Biography</label>
            <input name="biography" type="text" class="form-control" id="biography" value="{{$user->biography}}"
                   placeholder="Enter Biography">
        </div>

        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp"
                   placeholder="{{$user->email}}">
        </div>

        <div class="mb-3">
            <label for="password" class="form-label">New Password</label>
            <input name="password" type="password" class="form-control" id="password">
        </div>

        <div class="mb-3">
            <label for="picture" class="form-label">Picture</label>
            <input class="form-control" name="picture" type="file" class="form-control" id="picture">
        </div>

        <div class="mb-3">
            <label for="birthday" class="form-label">Birthday</label>
            <input name="birthday" type="date" class="form-control" id="birthday" value="{{$user->birthday}}">
        </div>

        <div>
            Roles
            <hr>
            @if($user->roles->contains('type','ADMIN'))
                <div class="form-check form-switch">
                    <input name="role[]" class="form-check-input" type="checkbox" role="switch"
                           id="flexSwitchCheckDefault" value="1" checked>
                    <label class="form-check-label" for="flexSwitchCheckDefault">ADMIN</label>
                </div>
            @else
                <div class="form-check form-switch">
                    <input name="role[]" class="form-check-input" type="checkbox" role="switch"
                           id="flexSwitchCheckDefault" value="1">
                    <label class="form-check-label" for="flexSwitchCheckDefault">ADMIN</label>
                </div>
            @endif

            @if($user->roles->contains('type','USER'))
                <div class="form-check form-switch">
                    <input name="role[]" class="form-check-input" type="checkbox" role="switch"
                           id="flexSwitchCheckDefault" value="2" checked>
                    <label class="form-check-label" for="flexSwitchCheckDefault">USER</label>
                </div>
            @else
                <div class="form-check form-switch">
                    <input name="role[]" class="form-check-input" type="checkbox" role="switch"
                           id="flexSwitchCheckDefault" value="2">
                    <label class="form-check-label" for="flexSwitchCheckDefault">USER</label>
                </div>
            @endif

            @if($user->roles->contains('type','MOD'))
                <div class="form-check form-switch">
                    <input name="role[]" class="form-check-input" type="checkbox" role="switch"
                           id="flexSwitchCheckDefault" value="3" checked>
                    <label class="form-check-label" for="flexSwitchCheckDefault">MODIFIER</label>
                </div>
            @else
                <div class="form-check form-switch">
                    <input name="role[]" class="form-check-input" type="checkbox" role="switch"
                           id="flexSwitchCheckDefault" value="3">
                    <label class="form-check-label" for="flexSwitchCheckDefault">MODIFIER</label>
                </div>
            @endif

        </div>

        <div class="d-flex justify-content-around">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a class="btn btn-dark" href="{{route('topic.index')}}">Back</a>
        </div>
    </form>
</div>



<style>
    .card {
        width: 210px;
        height: 280px;
        background: rgb(39, 39, 39);
        border-radius: 12px;
        box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.123);
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: flex-start;
        transition-duration: .5s;
    }

    .profileImage {
        background: linear-gradient(to right,rgb(54, 54, 54),rgb(32, 32, 32));
        margin-top: 20px;
        width: 170px;
        height: 170px;
        border-radius: 50%;
        box-shadow: 5px 10px 20px rgba(0, 0, 0, 0.329);
    }

    .textContainer {
        width: 100%;
        text-align: left;
        padding: 20px;
        display: flex;
        flex-direction: column;
        gap: 10px;
    }

    .name {
        font-size: 0.9em;
        font-weight: 600;
        color: white;
        letter-spacing: 0.5px;
    }

    .profile {
        font-size: 0.84em;
        color: rgb(194, 194, 194);
        letter-spacing: 0.2px;
    }

    .card:hover {
        background-color: rgb(43, 43, 43);
        transition-duration: .5s;
    }
</style>

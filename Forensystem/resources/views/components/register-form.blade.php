
<div class="d-flex justify-content-around my-5">
    <form class="form" action="{{route('register')}}" method="post" enctype="multipart/form-data">
        @csrf
        <p class="title">Register </p>
        <p class="message">Signup now and get full access to our app. </p>

        <label>
            <input name="username" class="input" type="text" placeholder="" required="" value="{{old('username')}}">
            <span>Username</span>
        </label>

        <label>
            <input name="email" class="input" type="email" placeholder="" required="" value="{{old('email')}}">
            <span>Email</span>
        </label>

        <label>
            <input name="password" class="input" type="password" placeholder="" required="">
            <span>Password</span>
        </label>

        <label>
            <input name="password_confirmation" class="input" type="password" placeholder="" required="">
            <span>Confirm password</span>
        </label>

        <div class="flex">
            <label>
                <span>Picture</span>
                <input class="form-control" name="picture" type="file" class="form-control" id="picture">
            </label>
            <label>
                <span>Birthday</span>
                <input name="birthday" type="date" class="form-control" id="birthday" value="{{old('birthday')}}">
            </label>
        </div>


        <label class="form-floating">
        <textarea name="biography" class="form-control" placeholder="Leave a comment here" id="floatingTextarea2"
                  style="height: 100px">
            {{old('biography')}}
        </textarea>
            <span>Biography</span>
        </label>


        <button class="submit" type="submit">Submit</button>
        <p class="signin">Already have an acount ? <a href="{{route('loginPage')}}">Signin</a></p>
    </form>

</div>


<style>
    .form {
        display: flex;
        flex-direction: column;
        gap: 10px;
        max-width: 50%;
        background-color: #fff;
        padding: 20px;
        border-radius: 20px;
        position: relative;
    }

    .title {
        font-size: 28px;
        color: #04ac08;
        font-weight: 600;
        letter-spacing: -1px;
        position: relative;
        display: flex;
        align-items: center;
        padding-left: 30px;
    }

    .title::before, .title::after {
        position: absolute;
        content: "";
        height: 16px;
        width: 16px;
        border-radius: 50%;
        left: 0px;
        background-color: #0f5e13;
    }

    .title::before {
        width: 18px;
        height: 18px;
        background-color: #04ac08;
    }

    .title::after {
        width: 18px;
        height: 18px;
        animation: pulse 1s linear infinite;
    }

    .message, .signin {
        color: rgba(88, 87, 87, 0.822);
        font-size: 14px;
    }

    .signin {
        text-align: center;
    }

    .signin a {
        color: royalblue;
    }

    .signin a:hover {
        text-decoration: underline royalblue;
    }

    .flex {
        display: flex;
        width: 100%;
        gap: 6px;
    }

    .form label {
        position: relative;
    }

    .form label .input {
        width: 100%;
        padding: 10px 10px 20px 10px;
        outline: 0;
        border: 1px solid rgba(105, 105, 105, 0.397);
        border-radius: 10px;
    }

    .form label .input + span {
        position: absolute;
        left: 10px;
        top: 15px;
        color: grey;
        font-size: 0.9em;
        cursor: text;
        transition: 0.3s ease;
    }

    .form label .input:placeholder-shown + span {
        top: 15px;
        font-size: 0.9em;
    }

    .form label .input:focus + span, .form label .input:valid + span {
        top: 0px;
        font-size: 0.7em;
        font-weight: 600;
    }

    .form label .input:valid + span {
        color: green;
    }

    .submit {
        border: none;
        outline: none;
        background-color: #04ac08;
        padding: 10px;
        border-radius: 10px;
        color: #fff;
        font-size: 16px;
        transform: 2.3s ease;
    }

    .submit:hover {
        background-color: rgb(18, 149, 31);
        cursor: pointer;
    }

    @keyframes pulse {
        from {
            transform: scale(0.9);
            opacity: 1;
        }

        to {
            transform: scale(1.8);
            opacity: 0;
        }
    }
</style>

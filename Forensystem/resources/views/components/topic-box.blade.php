<div class="task">
    <div class="tags">
        <span class="tag">
            <a class=" nav-link" href="{{route('board.topics',$topic->board->id)}}">
            {{$topic->board->name}}
            </a>
        </span>
    </div>

    <a class="nav-link" href="{{route('topic.show',$topic->id)}}">
        <p>{{$topic->title}} </p>
    </a>

    <div class="stats">
        <div>
            <div>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <g stroke-width="0" id="SVGRepo_bgCarrier"></g>
                    <g stroke-linejoin="round" stroke-linecap="round" id="SVGRepo_tracerCarrier"></g>
                    <g id="SVGRepo_iconCarrier">
                        <path stroke-linecap="round" stroke-width="2" d="M12 8V12L15 15"></path>
                        <circle stroke-width="2" r="9" cy="12" cx="12"></circle>
                    </g>
                </svg>
                {{$topic->created_at->format('Y-m')}}
            </div>
        </div>

        <div class="viewer">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                <g stroke-width="0"
                   id="SVGRepo_bgCarrier"></g>
                <g
                    stroke-linejoin="round" stroke-linecap="round" id="SVGRepo_tracerCarrier"></g>
                <g
                    id="SVGRepo_iconCarrier">
                    <path stroke-width="2" stroke="#ffffff"
                          d="M17 8C17 10.7614 14.7614 13 12 13C9.23858 13 7 10.7614 7 8C7 5.23858 9.23858 3 12 3C14.7614 3 17 5.23858 17 8Z"></path>
                    <path
                        stroke-linecap="round" stroke-width="2" stroke="#ffffff"
                        d="M3 21C3.95728 17.9237 6.41998 17 12 17C17.58 17 20.0427 17.9237 21 21"></path>
                </g>
            </svg>
            <span>
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><g stroke-width="0"
                                                                                           id="SVGRepo_bgCarrier"></g><g
                        stroke-linejoin="round" stroke-linecap="round" id="SVGRepo_tracerCarrier"></g><g
                        id="SVGRepo_iconCarrier"> <path stroke-width="2" stroke="#ffffff"
                                                        d="M17 8C17 10.7614 14.7614 13 12 13C9.23858 13 7 10.7614 7 8C7 5.23858 9.23858 3 12 3C14.7614 3 17 5.23858 17 8Z"></path> <path
                            stroke-linecap="round" stroke-width="2" stroke="#ffffff"
                            d="M3 21C3.95728 17.9237 6.41998 17 12 17C17.58 17 20.0427 17.9237 21 21"></path> </g></svg>
            </span>
            <span>+{{$topic->comments->count()}}</span>
        </div>
    </div>
</div>


<style>
    .task {
        position: relative;
        color: #2e2e2f;
        background-color: #fff;
        padding: 1rem;
        border-radius: 8px;
        box-shadow: rgba(99, 99, 99, 0.1) 0px 2px 8px 0px;
        margin-bottom: 1rem;
        border: 3px dashed transparent;
        max-width: 100%;
    }

    .task:hover {
        box-shadow: rgba(99, 99, 99, 0.3) 0px 2px 8px 0px;
        border-color: rgba(162, 179, 207, 0.2) !important;
    }

    .task p {
        font-size: 15px;
        margin: 1.2rem 0;
    }

    .tag {
        border-radius: 100px;
        padding: 4px 13px;
        font-size: 12px;
        color: #ffffff;
        background-color: #1389eb;
    }

    .tags {
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .stats {
        position: relative;
        width: 100%;
        color: #9fa4aa;
        font-size: 12px;
        display: flex;
        align-items: center;
        justify-content: space-between;
    }

    .stats div {
        margin-right: 1rem;
        height: 20px;
        display: flex;
        align-items: center;
        cursor: pointer;
    }

    .stats svg {
        margin-right: 5px;
        height: 100%;
        stroke: #9fa4aa;
    }

    .viewer span {
        height: 30px;
        width: 30px;
        background-color: rgb(28, 117, 219);
        margin-right: -10px;
        border-radius: 50%;
        border: 1px solid #fff;
        display: grid;
        align-items: center;
        text-align: center;
        font-weight: bold;
        color: #fff;
        padding: 2px;
    }

    .viewer span svg {
        stroke: #fff;
    }


</style>

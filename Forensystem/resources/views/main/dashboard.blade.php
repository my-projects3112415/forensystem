@php use Illuminate\Support\Collection; @endphp
@extends('layout.main')
@section('body')

    <div class="container">
        <x-messages-form/>

        <div class="d-flex justify-content-center">
            <x-search-input/>
            @auth
                <a href="{{route('topic.create')}}" class="text-success my-4">
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="25" height="25"
                         fill="currentColor"
                         class="bi bi-plus-circle"
                         viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14m0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16"/>
                        <path
                            d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4"/>
                    </svg>
                </a>
            @endauth
        </div>

        <div class="d-flex flex-column mx-4">

            @foreach($allTopics as $topic)
                <x-topic-box :topic="$topic"/>
            @endforeach

        </div>

        @if(! $allTopics instanceof Collection)
            <div class="d-flex justify-content-center">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="{{$allTopics->previousPageUrl()}}">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="{{$allTopics->nextPageUrl()}}">Next</a></li>
                    </ul>
                </nav>
            </div>
        @endif

    </div>


@endsection

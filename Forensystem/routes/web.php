<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BoardController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

















// Public

Route::view('/','components.login-form')->name('loginPage');
Route::post('/login', [AuthController::class,'login'])->name('login');
Route::view('/registerForm','components.register')->name('registerForm');
Route::view('/resetPasswordForm','components.forget-password')->name('resetPasswordForm');
Route::post('/usersRegister',[AuthController::class,'register'])->name('register');
Route::post('/sendResetMail',[AuthController::class,'sendResetMail'])->name('reset.mail');
Route::post('/verifyResetCode',[AuthController::class,'verifyResetCode'])->name('verifyCode');
Route::post('/resetPassword',[AuthController::class,'resetPassword'])->name('reset.password');
Route::get('/getBoardTopics/{board}',[TopicController::class,'boardTopics'])->name('board.topics');
Route::post('/search',[TopicController::class,'search'])->name('search');
Route::view('getForgetPassword','components.forget-password')->name('test');


// Authentication
Route::middleware('auth')->group(function (){
    Route::resource('notification', NotificationController::class);
    Route::get('/notificationOp/{op}',[NotificationController::class,'readOrDelete'])->name('notification.options');

    Route::get('/getProfilePage',[AuthController::class,'getProfilePage'])->name('get.Account');
    Route::put('/editProfile',[AuthController::class,'editAccount'])->name('edit.Account');

    Route::get('logout',[AuthController::class,'logout'])->name('logout');

    Route::resource('comment',CommentController::class);
    Route::resource('topic', TopicController::class)->except('index','show');

    Route::middleware('can:isAdmin')->group(function (){
        Route::resource('board', BoardController::class);
        Route::resource('users', UsersController::class);
    });
});

Route::resource('topic', TopicController::class)->only('index','show');

<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define('edit-comment',function (User $user,Comment $comment){
            return ($user->roles->contains('type','MOD') || $user->id == $comment->user_id);
        });

        Gate::define('isAdmin',function (User $user){
           return $user->roles->contains('type','ADMIN');
        });

        Gate::define('edit-topic',function (User $user,Topic $topic){
            return ($user->roles->contains('type','MOD') || $user->id == $topic->user_id);
        });

        Gate::define('edit-account', function (User $user, $userID) {
           return ($user->roles->contains('type','ADMIN') || $user->id == $userID);
        });

    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Comment;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $allTopics = Topic::paginate(10);
        return view('main.dashboard', compact('allTopics'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if (Auth::check()){
            $boards = Board::select('id', 'name')->get();
            return view('components.edit-topic-form', compact('boards'));
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $topic = new Topic();
        $topic->title = $request->title;
        $topic->board_id = $request->board;
        $topic->user_id = Auth::id();
        $topic->save();
        return to_route('topic.show', $topic->id);
    }

    /**
     * Display the specified resource.
     */
    public function show(Topic $topic)
    {
        return view('topics.main', compact('topic'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Topic $topic)
    {
        if (Gate::allows('edit-topic',$topic)){
            $boards = Board::select('id', 'name')->get();
            return view('components.edit-topic-form', compact('topic', 'boards'));
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Topic $topic)
    {
        if (Gate::allows('edit-topic',$topic)){
            $topic->board_id = $request->board ?? $topic->board->id;
            $topic->title = $request->title ?? $topic->title;
            $topic->save();
            return to_route('topic.show', $topic->id)->with('message', 'Topic' . $topic->title . " has been updated");
        }
        return abort(403);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Topic $topic)
    {
        if (Gate::allows('isMod',$topic)){
            $topic->delete();
            return to_route('topic.index');
        }
        return abort(403);

    }

    public function boardTopics(Board $board)
    {
        $allTopics = $board->topics()->paginate(10);
        return view('main.dashboard', compact('allTopics'));
    }

    public function search(Request $request)
    {
        $keyWord = $request->keyWord;
        $boardsTopics = Board::where('name', 'like', "%" . $keyWord . "%")->get()->flatMap->topics;
        $topics = Topic::where('title', 'like', "%" . $keyWord . "%")->get();
        $comments = Comment::where('content', 'like', "%" . $keyWord . "%")->get()->flatMap->topic;
        $allTopics = $topics->union($boardsTopics)->union($comments);

        return view('main.dashboard', compact('allTopics'));

    }

}

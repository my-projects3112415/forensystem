<?php

namespace App\Http\Controllers;

use App\Models\Board;
use Illuminate\Http\Request;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $boards = Board::paginate(10);
        return view('management.boards',compact('boards'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $board = new Board();
        $board->name = $request->name;
        $board->save();
        return back()->with('message','Board has been created');
    }

    /**
     * Display the specified resource.
     */
    public function show(Board $board)
    {
        $boardToEdit = $board;
        $boards = Board::paginate(10);
        return view('management.boards',compact('boardToEdit','boards'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Board $board)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Board $board)
    {
        $board->name = $request->name;
        $board->save();
        return to_route('board.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Board $board)
    {
        $board->delete();
        return to_route('board.index')->with('message','Board has been deleted');
    }
}

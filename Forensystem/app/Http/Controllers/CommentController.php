<?php

namespace App\Http\Controllers;

use App\Mail\NotificationMail;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\NotificationDTO;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $comment = new Comment();
            $comment->content = $request->commentContent;
            $comment->topic_id = $request->topicID;
            $comment->user_id = Auth::id();
            $comment->created_at = now();
            $comment->save();

            if (!$this->isOwnTopic($request->topicID)) {
                $topicName = Topic::where('id', '=', $request->topicID)->value('title');
                $owner = Topic::find($request->topicID)->user;

                $notification = new Notification();
                $notification->user_id = $owner->id;
                $notification->comment_id = $comment->id;
                $notification->topic_id = $request->topicID;
                $notification->created_at = now();
                $notification->save();


                $notification = new NotificationDTO(
                    $comment->user->name, $comment->content, $topicName);
                Mail::to($owner->email)->send(new NotificationMail($notification));
            }
            return back()->with('message', 'You Comment successfully on this Topic');
        }
        return abort(403);


    }

    /**
     * Display the specified resource.
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Comment $comment)
    {
        return view('components.comment-form', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Comment $comment)
    {
        if (Gate::allows('edit-comment', [$comment])) {
            $comment->content = $request->commentContent ?? $comment->content;
            $comment->created_at = now();
            $comment->save();
            return to_route('topic.show', $comment->topic->id)
                ->with('message', 'Comment has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comment $comment)
    {
        if (Gate::allows('edit-comment', [$comment])) {
            $topicID = $comment->topic->id;
            $comment->delete();
            return to_route('topic.show', $topicID)
                ->with('message', 'Comment has been deleted');
        }
    }


    // Help Methods
    private function isOwnTopic($topicID)
    {
        $userIdTopic = Topic::select('user_id')->where('id', '=', $topicID)->value('user_id');
        return Auth::id() == $userIdTopic;
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       $this->loadNotification();
        return back();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Notification $notification)
    {
        $topic = $notification->topic;
        $notification->read = true;
        $notification->save();
        $this->loadNotification();
        return view('topics.main', compact('topic'));

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Notification $notification)
    {
        $notification->read = true;
        $notification->save();
        $this->loadNotification();
        return back();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Notification $notification)
    {
        $notification->delete();
        return to_route('notification.index');

    }


    public function readOrDelete($op)
    {
        $notifications = Notification::where('user_id', '=', Auth::id())->get();
        switch ($op){
            case '-': {
                foreach ($notifications as $notification){
                    $notification->delete();
                }
            }break;
            case '+':{
                foreach ($notifications as $notification){
                    $notification->read = true;
                    $notification->save();
                }
            }break;
        }
        return to_route('notification.index');
    }

    private function loadNotification()
    {
        $notification = Notification::where('user_id', '=', Auth::id())->get();
        $unreadNotification = Notification::where('user_id', '=', Auth::id())->where('read', '=' ,0) ->get();
        $countUnread = count($unreadNotification);
        Session::put('notification', $notification);
        Session::put('unread', $countUnread);
    }
}

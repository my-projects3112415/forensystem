<?php

namespace App\Http\Controllers;

use App\Mail\ResetPasswordMail;
use App\Models\Notification;
use App\Models\Role;
use App\Models\User;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use League\CommonMark\Extension\CommonMark\Node\Inline\Strong;

class AuthController extends Controller
{
    // Login & Logout
    public function login(Request $request)
    {
        $validation = $request->validate([
            'email' => 'required', 'email',
            'password' => 'required',
        ]);

        if (Auth::attempt($validation)) {
            session()->regenerate();
            $this->loadNotification();
            return to_route('topic.index');
        }else return back()->withErrors(['input' => 'Invalid Credentials']);
    }
    public function logout()
    {
        session()->invalidate();
        session()->regenerateToken();
        session()->flush();
        return to_route('loginPage');
    }


    // Get Pages
    public function getProfilePage()
    {
        $user = Auth::user();
        return view('components.register', compact('user'));
    }


    // Register and Edit using same View
    public function register(Request $request)
    {
        $validation = $request->validate([
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'birthday' => 'required|date',
            'password' => 'required|confirmed',
        ]);
        $userRole = Role::select('id')->where('type', 'USER')->get();

        $user = new User();
        $user->name = $validation['username'];
        $user->email = $validation['email'];
        $user->password = Hash::make($validation['password']);
        $user->birthday = $validation['birthday'];

        if ($request->filled('biography')) $user->biography = $request->biography;
        $this->imageUpload($request,$user);
        $user->save();


        $user->roles()->attach($userRole);

        return to_route('loginPage')
            ->with('message', 'User ' . $user->name . " has been created");

    }

    public function editAccount(Request $request)
    {
        if (Gate::allows('edit-account',$request->id)){
            $user = User::find($request->id);
            $user->name = $request->username ?? $user->username;
            $user->biography = $request->biography ?? $user->biography;
            if ($request->filled('email')) {
                $request->validate(['email' => 'email|unique:users,email']);
                $user->email = $request->email;
            }
            if ($request->filled('password')) $user->password = Hash::make($request->password);
            if ($request->birthday !== $user->birthday) {
                $request->validate(['birthday' => 'date']);
                $user->birthday = $request->birthday;
            }
            $this->setRoles($request);
            $this->imageUpload($request,$user);
            $user->save();
            return to_route('topic.index')->with('message', 'User ' . $user->name . ' has been updated');
        }
        return  abort(403);
    }

    // Reset Email Process
    public function sendResetMail(Request $request)
    {
        $faker = Factory::create();
        $validation = $request->validate(['email'=>'required|email|exists:App\Models\User,email']);
        $isEmailExist = User::where('email',$validation['email'])->exists();
        $newToken = $faker->unique()->randomNumber(4);

        if ($isEmailExist){
            User::where('email',$validation['email'])->update(['reset_password_token' => $newToken]);
            Mail::to($validation['email'])->send(new ResetPasswordMail($newToken));
            return view('components.forget-password', ['isCodeSent' => true]);
        }
        return back()->withErrors('This email dose not exist');
    }

    public function verifyResetCode(Request $request)
    {
        $concatenatedDigits = implode('', $request->resetCode);
        $resetCode = $concatenatedDigits;
        $isResetCodeExist = User::where('reset_password_token',$resetCode)->exists();
        if ($isResetCodeExist){
            $userID = User::where('reset_password_token',$resetCode)->value('id');
            return view('components.reset-password-form',compact('userID'));
        }
        return  to_route('test')
            ->with('isCodeSent',true)
            ->withErrors('Invalid Error');
    }

    public function resetPassword(Request $request)
    {
        $validation = $request->validate(['password' => 'required|confirmed']);
        User::where('id', $request->userID)->update([
            'password'=> Hash::make($validation['password']),
            'reset_password_token' => null
            ]);
        return to_route('loginPage')->with('message','Password has been successfully changed ' );
    }




    // Help Methods
    private function imageUpload(Request $request, User $user)
    {
        if ($request->hasFile('picture')){
            $request->validate(['picture' => 'mimes:jpeg,png']);
            $userOldPicture = $user->profilePic;
            $fullPath = "public/images/".$userOldPicture;
            if (Storage::exists($fullPath)){
                Storage::delete($fullPath);
            }
            $hashedPictureName = $request->file('picture')->hashName();
            $request->file('picture')->storeAs('public/images',$hashedPictureName);
            $user->profilePic = $hashedPictureName;
        }
    }

    private function setRoles(Request $request)
    {
        if (Auth::user()->roles->contains('type','ADMIN')){
            $rolesList = $request->role;
            $user = User::find($request->id);
            $user->roles()->sync($rolesList);
        }
    }

    private function loadNotification()
    {
        $notification = Notification::where('user_id', '=', Auth::id())->get();
        $unreadNotification = Notification::where('user_id', '=', Auth::id())->where('read', '=' ,0)->get();
        $countUnread = count($unreadNotification);
        Session::put('notification', $notification);
        Session::put('unread', $countUnread);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Board extends Model
{
    use HasFactory;


    // Which Topics has this Board 1 : n
    public function topics(): HasMany
    {
        return $this->hasMany(Topic::class);
    }

}

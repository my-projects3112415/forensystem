<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use HasFactory;


    // In which Board is this Topic 1 : n
    public function board() : BelongsTo
    {
        return $this->belongsTo(Board::class);
    }

    // Which Comments has this Topic 1 : n
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    // Which user created this Topic 1 : n
    public function user(): BelongsTo
    {
        return  $this->belongsTo(User::class);
    }

    public function notifications(): HasMany
    {
        return $this->hasMany(Notification::class);
    }


}

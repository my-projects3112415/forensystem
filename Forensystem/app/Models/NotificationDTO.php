<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationDTO extends Model
{
    use HasFactory;


    public function __construct(
        public $commentBy,
        public $comment,
        public $topicTitle
    )
    {
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory;


    // n : m
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'role_of_users',
            'role_id',
            'user_id',
        );
    }
}

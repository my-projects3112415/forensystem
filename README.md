Project Requirements:

- **Name:**
  The project is named "Community Platform."

- **Description:**
  The community platform encompasses the management of users, roles, boards, topics, and comments. Users can register, have different roles, and perform various actions based on their roles. The platform allows the creation, editing, and deletion of boards, topics, and comments, with specific permissions based on user roles.

- **Structure:**
  The database schema includes tables for users, roles, boards, topics, and comments. There are clear relationships between the tables governing interactions between users, boards, and content. The platform has a clear user interface structure with pages for user management, board management, creation of new topics and comments, as well as profile pages.

- **Functionality:**
  - **User Management:**
    - The admin can manage users, including CRUD operations and role assignments.
    - Registration is possible for users, while admins and mods must be created by an admin.
    - Secure login, password reset functionality via email.

  - **Board Management:**
    - Admins can create, edit, and delete boards.
    - Mods have CRUD rights for their own topics and comments, as well as editing and deletion rights for topics and comments created by other users.
    - Users can create, edit, and delete their own topics and comments.

  - **Content Creation:**
    - Moderators and users can create new topics and respond to them, including a quote function.
    - Reply options are available for existing comments.

  - **Profile Management:**
    - Users can edit their profile information, including profile picture, biography, and birthday.
    - Each user has a profile page displaying all created topics and comments.

  - **Security and Feedback:**
    - Input validation is implemented.
    - Feedback on actions (e.g., successful comment creation) is provided.
